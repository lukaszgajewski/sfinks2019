import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb

sb.set()

states = np.load("data/mean_states_test.npy")
opinions = states[:, 0]
news_spins = states[:, 1]
news_charms = states[:, 2]
plt.plot(opinions, "-", label="agents opinion")
plt.plot(news_spins, "-", label="news spin", alpha=0.5)
plt.plot(news_charms, "-", label="news attractivness")
plt.legend()
plt.xlabel("MC steps")
plt.ylabel("mean value")
plt.show()
