import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def get_cascades():
    states = np.load('data/out.npy', allow_pickle=True)
    pseudo_cascade_depths = []
    science_cascade_depths = []
    for state in states:
        state = pd.DataFrame(state)
        pseudo_cascade_depths += list(state.loc[state[0] == 1][2])
        science_cascade_depths += list(state.loc[state[0] == -1][2])
    return np.array(pseudo_cascade_depths), np.array(science_cascade_depths)


def ccdfs():
    pseudo_cascade_depths, science_cascade_depths = get_cascades()
    pseudo_ccdf = (-np.arange(len(pseudo_cascade_depths)) /
                   len(pseudo_cascade_depths)+1)*100
    pseudo_b = np.sort(pseudo_cascade_depths)
    science_ccdf = (-np.arange(len(science_cascade_depths)) /
                    len(science_cascade_depths)+1)*100
    science_b = np.sort(science_cascade_depths)
    return pseudo_ccdf, pseudo_b, science_ccdf, science_b


pseudo_ccdf, pseudo_b, science_ccdf, science_b = ccdfs()
plt.loglog(pseudo_b, pseudo_ccdf, 'r-', label='pseudo')
plt.loglog(science_b, science_ccdf, 'b-', label='science')
plt.grid()
plt.legend()
plt.xlabel('cascade depth')
plt.ylabel('ccdf %')
plt.show()


# cmap = plt.get_cmap('cool')
# colors = cmap(np.linspace(0, 1.0, len(states)))
