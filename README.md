# PSEUDOSCIENCE CONTAGION MODEL - SFINKS 2019

## Model Description

Agent based model of science and pseudo-science news spreading in social media.

News have two attributes:  
m = {-1, 1} - where -1 means its a pseudoscience, 1 the opposite. here:"spin"  
$\pi$ = [0, 1] - how sexy, charming, attratctive, whatever the message/news/post is. here:"charm"  
We keep track of where the news have been to compute cascade

Every agent has an "opinion" s = [-1, 1]
and an attribute $\alpha$ = [0, 1] representing their vulnerability to change.  
In this implementation we use "1-$\alpha$" as a resistance to change,
i.e.:"adherence" (to their current opinion).  
They have a buffer - "news-feed" - that is a list of news they are perusing
and potentially sending to their friends.

There is a global parameter $\beta$ - "impact" > 0 that
describes impact of the messages to every agent’s opinion.

Each micro step (round):

- add news to the system
- choose an agent at random
- update their state with the formula:
  
  ![opinion formula](opinion_formula.png)
- choose some of their news according to:
  
  ![news formula](news_formula.png)
- send those news to all of agent's friends
- clear agent's buffer

## Running

With default values:

```unix
cd sfinks2019
python src/model.py
```

<!-- that will save two files: *data/cascades_1.npy* and *data/mean_states_1.npy*   -->
That will save a file: *data/mean_states_test.npy*
(you need to create sfinks2019/data folder yourself)  
<!-- and then you can run *[scripts/ccdf.py](scripts/ccdf.py)* to
produce a complementary cumulative distribution function of cascades' depths.   -->
and you can run *[scripts/plot_states_change.py](scripts/plot_states_change.py)* to see how mean opinion, spin and charm changed in time throughout the system evolution.

To run with your own parameter values modify this bit in *[src/model.py](src/model.py)*:

```python
if __name__ == "__main__":
    main(
        graph=nx.barabasi_albert_graph(100, 4),
        news_count=10 ** 2,
        news_feed_limit=100,
        n_steps=10 ** 3,
        impact=1.0,
        pseudo_to_science_ratio=0.7,
        output_suffix="_test",
    )
```

## Dependencies

- python3
- networkx
- numpy
- matplotlib
- tqdm
