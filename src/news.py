# -*- coding: utf-8 -*-
"""Module containing news related logic

"""
import networkx as nx
import numpy as np


def clear_news_feed(graph: nx.Graph, agent: int) -> nx.Graph:
    """clear agent's news feed

    Parameters
    ----------
    graph : networks.Graph
        the system
    agent : int
        agent's id whose feed we want to clear
    Returns
    -------
    networkx.Graph
        system with agent's neews feed cleared
    """
    graph.nodes[agent]["news-feed"] = []
    return graph


def prepare_news_to_send(
    graph: nx.Graph, agent: int, spins: np.ndarray, charms: np.ndarray
) -> list:
    """return list of news that given agent will send to their friends

    Parameters
    ---------
    graph : networkx.Graph
        the system
    agent : int
        sending agent's id
    spins : np.ndarray
        sending agent's news feed spins
    charms : np.ndarray
        sending agent's news feed charms

    Returns
    -------
    list
        list of news to send by the agent
    """
    s = graph.nodes[agent]["opinion"]
    B = graph.graph["impact"]
    news = graph.nodes[agent]["news-feed"]
    norm = np.sum(charms * np.exp(B * spins * s))
    share_probabilities = charms * np.exp(B * spins * s) / norm
    news_to_send_mask = np.where(share_probabilities < np.random.random(len(news)))
    news_to_send = list(np.array(news)[news_to_send_mask])
    return news_to_send


def send_news(graph: nx.Graph, agent: int, news_to_send: list) -> nx.Graph:
    """send chosen news to agent's friends and
    update given news' visited nodes set

    Parameters
    ----------
    graph : networkx.Graph
        the system
    agent : int
        id of an agent that sends their news
    news_to_send : list
        list of news the agent sends

    Returns
    -------
    networkx.Graph
        system with chosen news sent by the agent

    See also
    --------
    share_news
    """
    for friend in graph[agent]:
        graph.nodes[friend]["news-feed"] += news_to_send
        feed = graph.nodes[friend]["news-feed"]
        if len(feed) > graph.graph["news-feed-limit"]:
            n_newest = len(feed) - graph.graph["news-feed-limit"]
            graph.nodes[friend]["news-feed"] = feed[n_newest:]
    for news in news_to_send:
        news["visited-nodes"].update(graph[agent])
    return graph


def share_news(
    graph: nx.Graph, agent: int, spins: np.ndarray, charms: np.ndarray
) -> nx.Graph:
    """share news of agent to their friends, that is prepare and send them

    Parameters
    ----------
    graph : nx.Graph
        the system
    agent : int
        id of an agent that will share some of their news feed
    spins : np.ndarray
        agent's news feed spins
    charms : np.ndarray
        agent's news feed charms

    Returns
    -------
    networkx.Graph
        system after the news has been shared (that is chosen and sent)
    """
    news_to_send = prepare_news_to_send(graph, agent, spins, charms)
    if news_to_send:
        send_news(graph, agent, news_to_send)
    return graph
