# -*- coding: utf-8 -*-
"""Module containing system related logic

"""

import networkx as nx
import numpy as np
from agent import update_opinion
from news import share_news, clear_news_feed


def get_spins_charms(graph: nx.Graph, agent: int) -> tuple:
    """return tuple of arrays (spins, charms) for given agent's news feed

    Parameters
    ----------
    graph : networkx.Graph
        the system
    agent : int
        agent's id whose news feed's spins and charms we want

    Returns
    -------
    tuple
        tuple of numpy.ndarrays (spins, charms)
    """
    spins = np.fromiter(
        [n["spin"] for n in graph.nodes[agent]["news-feed"]],
        np.int,
        count=len(graph.nodes[agent]["news-feed"]),
    )
    charms = np.fromiter(
        [n["charm"] for n in graph.nodes[agent]["news-feed"]],
        np.float,
        count=len(graph.nodes[agent]["news-feed"]),
    )
    return spins, charms


def generate_adherences_opinions(graph: nx.Graph) -> tuple:
    """return newly created adherences and opinions for agents

    Parameters
    ----------
    graph : networkx.Graph
        the system

    Returns
    -------
    tuple
        tuple of dicts (adherences{node: adherence}, opinions{node: opinion})
    """
    # adherences = dict(zip(graph.nodes, np.random.random(len(graph))))
    adherences = dict(zip(graph.nodes, np.random.beta(5.0, 5.0, len(graph))))
    # opinions = np.random.random(len(graph)) * (1 - (-1)) + (-1)  # [-1, 1]
    opinions = dict(zip(graph.nodes, np.random.beta(5.0, 5.0, len(graph)) * 2 - 1))
    return adherences, opinions


def generate_news(graph: nx.Graph) -> list:
    """add list of news to the graph object's attribute and return that list

    Parameters
    ----------
    graph : networkx.Graph

    Returns
    -------
    list
        of news with length graph.graph["news-count"];
        each element is a dict representing a singular instance (piece) of news
    """
    news_count = graph.graph["news-count"]
    p = graph.graph["pseudo_to_science_ratio"]
    spins = np.random.choice([-1, 1], news_count, p=[p, 1 - p])
    # charms = np.random.random(news_count)
    charms = np.random.beta(0.5, 1.5, news_count)
    news = [
        {"spin": spin, "charm": charm, "visited-nodes": set(), "source": None}
        for spin, charm in zip(spins, charms)
    ]
    graph.graph["news"] += news
    return news


def distribute_news(graph: nx.Graph) -> nx.Graph:
    """return the system with newly generated news spread around in it

    Parameters
    ----------
    graph : networkx.Graph

    Returns
    -------
    networkx.Graph
        system with the fresh news generated and distributed among agents
    """
    chosen_nodes = np.random.choice(graph.nodes, graph.graph["news-count"])
    generated_news = generate_news(graph)
    for news, node in zip(generated_news, chosen_nodes):
        graph.nodes[node]["news-feed"].append(news)
        news["visited-nodes"].add(node)
        news["source"] = node
    return graph


def system_init(
    graph: nx.Graph,
    news_count: int,
    news_feed_limit: int,
    impact: float,
    pseudo_to_science_ratio: float,
) -> nx.Graph:
    """initialise system, i.e.:
    assign adherences and opinions to agents,
    initialise their news feeds as empty lists
    assign impact, amount of news generated each round,
    news feed size, and list of news (to track their history)
    as graph attributes
    calculate shortest path length between each node for later use in
    cascade length computation

    Parameters
    ----------
    graph : networkx.Graph
        system topology
    news_count : int
        number of news appearing in the system each round (micro step)
    news_feed_limit : int
        agent's news feed (buffer) size
    impact : float
        impact of news on agents

    Returns
    -------
        initialised system
    """
    adherences, opinions = generate_adherences_opinions(graph)
    nx.set_node_attributes(graph, adherences, "adherence")
    nx.set_node_attributes(graph, opinions, "opinion")
    nx.set_node_attributes(graph, {n: [] for n in graph.nodes}, "news-feed")
    graph.graph["impact"] = impact
    graph.graph["news"] = []
    graph.graph["news-count"] = news_count
    graph.graph["news-feed-limit"] = news_feed_limit
    graph.graph["distances"] = dict(nx.all_pairs_shortest_path_length(graph))
    graph.graph["pseudo_to_science_ratio"] = pseudo_to_science_ratio
    return graph


def system_update(graph: nx.Graph) -> nx.Graph:
    """update graph, i.e.:
    add news to the system,
    choose an agent,
    update his opinion,
    potentially make him share his news,
    clear their buffer

    this is a round (micro step)

    Parameters
    ----------
    graph : networkx.Graph
        the system

    Returns
    -------
    networkx.Graph
        updated system
    """
    graph = distribute_news(graph)
    agent = np.random.randint(len(graph))
    spins, charms = get_spins_charms(graph, agent)
    if spins.size > 0:
        update_opinion(graph, agent, spins, charms)
        share_news(graph, agent, spins, charms)
        clear_news_feed(graph, agent)
    return graph


def system_status(graph: nx.Graph) -> np.ndarray:
    """return array of (spin, charm, cascade depth) for each news and
    delete from memory (tracking) those news that no longer can be shared

    Parameters
    ----------
    graph : networkx.Graph
        the system

    Returns
    -------
    numpy.ndarray
        array of shape=(number of news in the system, 3) where columns are:
        spin, charm, cascade depth
    """
    states = []
    for agent in graph:
        sc = get_spins_charms(graph, agent)
        if sc[0].size > 0:
            means = np.mean(np.vstack(sc), axis=1)
        else:
            means = [np.nan, np.nan]
        states.append((graph.nodes[agent]["opinion"], means[0], means[1]))
    states = np.array(states)
    mean_state = np.nanmean(states, axis=0)

    news = [
        [
            n["spin"],
            n["charm"],
            max(
                [
                    graph.graph["distances"][n["source"]][node]
                    for node in n["visited-nodes"]
                ]
            ),
        ]
        for n in graph.graph["news"]
    ]
    news = np.array(news)
    # remove those news that are no longer roaming around
    news_in_system = {id(news) for n in graph for news in graph.nodes[n]["news-feed"]}
    graph.graph["news"] = [
        news for news in graph.graph["news"] if id(news) in news_in_system
    ]
    return news, mean_state
