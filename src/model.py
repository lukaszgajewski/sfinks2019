# -*- coding: utf-8 -*-
"""Module containing main Monte Carlo simulation loop

"""
import networkx as nx
import numpy as np
from tqdm import trange
from system import system_init, system_update, system_status


def macro_step(graph: nx.Graph) -> np.ndarray:
    """return system's status afetr a macro step (Monte Carlo simulation step),
    i.e.: execute micro step (round) as many times as the size of the system
    and return system's status

    Parameters
    ----------
    graph : networkx.Graph
        the system

    Returns
    -------
    numpy.ndarray
        system status after a Monte Carlo time step
    """
    for _ in trange(len(graph), position=1, leave=False):
        system_update(graph)
    return system_status(graph)


def main(
    graph: nx.Graph,
    news_count: int,
    news_feed_limit: int,
    n_steps: int,
    impact: float,
    pseudo_to_science_ratio=0.5,
    output_suffix: str = "_1",
):
    """execute the simulation and save results to the output files:
    data/cascades+output_suffix :
    news cascade information: array of arrays. Column of inner arrays are
    spin, charm, cascade depth. Each row is different news.
    Each array is a point in time.
    News are not uniquely identifiable by the row index.
    data/mean_states+output_suffix :
    array where each row is a point in time.
    Columns are opinion, spin, charm.


    Parameters
    ----------
    graph : networkx.Graph
        system topology
    news_count : int
        number of news generated each round
    n_steps : int
        number of Monte Carlo steps
    output_suffix : str
        output name suffix
    """
    graph = system_init(
        graph, news_count, news_feed_limit, impact, pseudo_to_science_ratio
    )
    states = [macro_step(graph) for _ in trange(n_steps, position=0, leave=True)]
    # cascades = np.array([state[0] for state in states])
    mean_states = np.array([state[1] for state in states])
    # np.save("data/cascades" + output_suffix, np.array(cascades))
    np.save("data/mean_states" + output_suffix, np.array(mean_states))


if __name__ == "__main__":
    main(
        graph=nx.barabasi_albert_graph(100, 4),
        news_count=10 ** 1,
        news_feed_limit=100,
        n_steps=10 ** 3,
        impact=1.0,
        pseudo_to_science_ratio=0.7,
        output_suffix="_test",
    )
