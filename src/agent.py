# -*- coding: utf-8 -*-
"""Module containing agent related logic

"""
import networkx as nx
import numpy as np


def update_opinion(
    graph: nx.Graph, agent: int, spins: np.ndarray, charms: np.ndarray
) -> nx.Graph:
    """return graph with agents opinions (states) updated

    Parameters
    ----------
    graph : networkx.Graph
        the system
    agent : int
        agent's id that we want to update opinion of
    spins : np.ndarray
        array of agent's news' spins
    charms : np.ndarray
        array of agent's news' charms

    Returns
    -------
    networkx.Graph
        system with given agent's updated opinion
    """
    a = 1 - graph.nodes[agent]["adherence"]  # vulnerability
    s = graph.nodes[agent]["opinion"]
    B = graph.graph["impact"]
    norm = np.sum(charms * np.exp(B * spins * s))
    convincing_strength = np.sum(spins * charms * np.exp(B * spins * s))
    s_new = (1 - a) * s + a * convincing_strength / norm
    graph.nodes[agent]["opinion"] = s_new
    return graph
